```markdown
# MASdex

MASdex is a mobile Pokédex application developed in Kotlin for Android, leveraging the extensive PokéAPI to provide detailed information about Pokémon to enthusiasts and researchers alike. Designed with Professor Oak's needs in mind, MASdex aims to be the ultimate tool for Pokémon identification and research, featuring offline capabilities, filtering options, and a favorites system.

## Getting Started

### Prerequisites

- Android Studio
- Git
- Kotlin

### Installation

Clone this repository and import it into Android Studio.

```bash
git clone https://gitlab.com/acevedosanchezmanuel/masdex.git
```

## Features

- Display a list of Pokémon with images, names, and IDs.
- Filter Pokémon by type and name.
- View detailed information about each Pokémon by tapping on its entry.
- Offline viewing capabilities for accessing Pokémon data without an internet connection.
- Mark and filter favorite Pokémon, accessible even in offline mode.

## Technologies Used

- Kotlin for the main programming language.
- Retrofit for API calls to PokéAPI.
- Room for local database management.
- ViewModel and LiveData for managing UI-related data in a lifecycle-conscious way.
- Glide or Coil for image loading and caching.
- Dagger Hilt or Koin for dependency injection.

## Usage

After installing MASdex on your Android device, you can start exploring the world of Pokémon. Use the search and filter features to narrow down your discoveries, tap on a Pokémon to view more detailed information, or mark your favorites for quick access later.

## Contributing

Contributions are what make the open-source community such an amazing place to learn, inspire, and create. Any contributions you make are **greatly appreciated**.

If you have a suggestion that would make this better, please fork the repo and create a pull request. You can also simply open an issue with the tag "enhancement".

Don't forget to give the project a star! Thanks again!

1. Fork the Project
2. Create your Feature Branch (`git checkout -b feature/AmazingFeature`)
3. Commit your Changes (`git commit -m 'Add some AmazingFeature'`)
4. Push to the Branch (`git push origin feature/AmazingFeature`)
5. Open a Pull Request

## License

Distributed under the MIT License. See `LICENSE` for more information.

## Authors and Acknowledgment

- **Manuel Acevedo Sánchez** - _Initial work_ - [GitHubProfile](https://gitlab.com/acevedosanchezmanuel)

Special thanks to the PokéAPI for providing the data used in this project.

## Project Status

MASdex is currently in development. Future updates will include advanced filtering options, improved offline capabilities, and integration with additional Pokémon data sources.
```
